{$R+}
uses video,crt,SysUtils;
var
  w, h : word;
type
  daint = array of integer;
  dastr = array of string;
  dalw = array of longword;
  {$I lib/base/base.pas}
  {$I lib/asciiGL/asciiGL.pas}
  {$I lib/asciiGL_scene/asciiGL_scene.pas}
  {$I lib/res_edit/res_edit.pas}
begin
  getWHscreen(w,h);
  if paramstr(1) <> ''
  then res_editor(paramstr(1),w,h-1)
  else writeln ('usage: res_editor "path_to_file"');
  writeln();
end.
