procedure ascii_art_animation_file_new (path : ansistring; nFrames : longword; fps : word);
var
  f : text;
  n : longword;
  dirFrames, dir,filename : ansistring;
begin
  for n := length (path) downto 1 do
  begin
    if path[n] = '\' then break;
    if path[n] = '/' then break;
  end;
  dir := copy(path, 1, n);
  filename := copy(path, n+1, length (path));
  
  assign(f, path);
  rewrite(f);
  writeln(f, 'filename = ',filename,';');
  writeln(f, 'dirFrames = ',filename+'_frames/',';');
  writeln(f, 'nFrames = ',nFrames,';');
  writeln(f, 'fps = ',fps,';');
  dirFrames := dir+filename+'_frames/';
  if not DirectoryExists(dirFrames)
  then mkdir(dirFrames);
  for n := 0 to nFrames-1 do ascii_art_file_new(dirFrames+intToStr(n)+'.aa');
  close(f);
end;
