// #FIX_ME
procedure data_add_point (var data : daint; var dataOffset : dalw;  id, x, y, c, s : integer);
var
  data_tmp : daint;
  ofs, size, n : longword;
begin
  log_msg('bugs','ok-1 '+inttostr(id));
  ofs := dataOffset[id];
  size := data[ofs+1];
  setlength(data_tmp, 5);
  data_tmp[0] := 1;
  data_tmp[1] := x;
  data_tmp[2] := y;
  data_tmp[3] := c;
  data_tmp[4] := s;
  data[ofs+1] := size + 5;
  log_msg('bugs','ok-2 '+inttostr(ofs)+' '+inttostr(size));
  array_data_insert(data, data_tmp, ofs+size+2);
  log_msg('bugs','ok-3');
  id := id + 1;
  for n := id to length(dataOffset)-1 do
    dataOffset[n] := dataOffset[n] + 5;
end;
