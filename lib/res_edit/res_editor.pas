procedure res_editor(path : ansistring; w, h : word);
var
  filetype, str : string;
  xCam, yCam, xCur, yCur, color_set, color, rpX,rpY, rgX, rgY :integer;
  exit_editor, reload : boolean;
  frA, frB, scn, res_data : array of integer;
  res_dataOffset : array of longword;
  res_idAssociation : array of string;
  key : char;
  n, id : longword;
  file_n : word;
begin

  repeat
    file_n := 1;
    clrscr;
    reload := false;
    case path[length(path)-1]+path[length(path)] of
    'aa': filetype := 'ascii_art';
    'an': filetype := 'ascii_art_animation'
    else filetype := 'text_art';
    end;
    if not fileExists (path)
    then case filetype of
    'ascii_art' : ascii_art_file_new(path);
    'ascii_art_animation' : ascii_art_animation_file_new(path,1,25);
    'text_art' : text_file_new(path);
    end;

    case filetype of
    'ascii_art' : begin
      initFrame(frA, 1, w, h);
      initFrame(frB, 1, w, h);
      setlength(scn,0);
      setlength(res_data,0);
      setlength(res_dataOffset,0);
      setlength(res_idAssociation,0);
      dataLoad (res_data, res_dataOffset,  res_idAssociation, 'res/cur.dat', 'cur');
      dataLoad (res_data, res_dataOffset,  res_idAssociation, path, 'res');
      setlength(scn, 12);
      scn[0] := 0;
      scn[1] := 0;
      scn[2] := 0;
      scn[3] := 1;
      scn[4] := 0;
      scn[5] := 0;
      scn[6] := file_n;
      scn[7] := 0;
      scn[8] := 0;
      scn[9] := 0;
      scn[10] := 0;
      scn[11] := 0;
      xCur := 0;
      yCur := 0;
      xCam := 0;
      yCam := 0;
      id := 1;
      exit_editor := false;
      color_set := 0;
      color := 1;
      repeat
        if keypressed
        then begin
          key := readkey;
          case key of
            #0 : case readkey of
              #73 : file_n := file_n + 1;
              #81 : file_n := file_n - 1;
              #72 : yCur := yCur - 1;
              #80 : yCur := yCur + 1;
              #75 : xCur := xCur - 1;
              #77 : xCur := xCur + 1;
              #130 : color_set := 0;
              #131 : color_set := 8;
              #120 : color := 0 + color_set;
              #121 : color := 1 + color_set;
              #122 : color := 2 + color_set;
              #123 : color := 3 + color_set;
              #124 : color := 4 + color_set;
              #125 : color := 5 + color_set;
              #126 : color := 6 + color_set;
              #127 : color := 7 + color_set;
            end;
          #3 : begin
              window(round(w/2)-15, round(h/2)-2,round(w/2)+15,round(h/2)+2);
              textbackground(1);
              textcolor(14);
              clrscr;
              gotoxy(5,3);
              write('  Exit? (y/n)');
              if readkey = 'y'
              then exit_editor := true;
              initFrame(frB, 1, w, h);
              window (1,1,w,h);
              textbackground(0);
              textcolor(7);
              clrscr;
            end;
          #15 : begin
            window(round(w/2)-15, round(h/2)-2,round(w/2)+15,round(h/2)+2);
            textbackground(1);
            textcolor(14);
            clrscr;
            writeln ('Enter point of rotation.');
            write ('x: ');
            readln(rpX);
            write ('y: ');
            readln(rpY);
            res_data[res_dataOffset[id]+4] := rpX;
            res_data[res_dataOffset[id]+5] := rpY;
            initFrame(frB, 1, w, h);
            window (1,1,w,h);
            textbackground(0);
            textcolor(7);
            clrscr;

          end;
          #17 : begin
            window(round(w/2)-15, round(h/2)-2,round(w/2)+15,round(h/2)+2);
            textbackground(1);
            textcolor(14);
            clrscr;
            writeln ('import from file');
            write (': ');
            readln(str);
            res_data[res_dataOffset[id]+5] := rpY;
            initFrame(frB, 1, w, h);
            window (1,1,w,h);
            textbackground(0);
            textcolor(7);
            clrscr;
            text_art_import(str, res_data, res_dataOffset, res_idAssociation, 'imp');
          end;
          #22 : begin
            window(round(w/2)-15, round(h/2)-2,round(w/2)+15,round(h/2)+2);
            textbackground(1);
            textcolor(14);
            clrscr;
            writeln ('Enter range of visp.');
            write ('rgX: ');
            readln(rgX);
            write ('rgY: ');
            readln(rgY);
            res_data[res_dataOffset[id]+2] := rgY;
            res_data[res_dataOffset[id]+3] := rgY;
            initFrame(frB, 1, w, h);
            window (1,1,w,h);
            textbackground(0);
            textcolor(7);
            clrscr;
          end;
          #19 : begin
            data_save(path, res_data, res_dataOffset, 1);
            reload := true;
          end;
          else begin
            if key in [#32..#126]
              then begin
                data_add_point(res_data,res_dataOffset,1,xCur,yCur,color,ord(key));
              end;
            end;
          end;
        end;
        scn[1] := xCur;
        scn[2] := yCur;
        scn[6] := file_n;
        if xCur-xCam < round(w/2)-round(w/3)
        then xCam := xCam - 1;
        if yCur-yCam < round(h/2)-round(h/3)
        then yCam := yCam - 1;
        if xCur-xCam > round(w/2)+round(w/3)
        then xCam := xCam + 1;
        if yCur-yCam > round(h/2)+round(h/3)
        then yCam := yCam + 1;
        scene_rend (frA , scn, res_data, res_dataOffset, xCam, yCam);
       // setChXY(frA, xCur, yCur, 2, ord('+'));
        setText(frA,2,h-2,18,6,'FL '+intToStr(file_n)+'/'+intToStr(length(res_dataOffset)));
        setText(frA,2,h-1,18,6,'ResEdit  cur:'+intToStr(xCur)+' '+intToStr(yCur));
        setText(frA,w-30,h-1,10,6,'cam:'+intToStr(xCam)+' '+intToStr(yCam));
        setText(frA,w-10,h-1,10,6,'win:'+intToStr(w)+' '+intToStr(h));
        setText(frA,w-10,h-2,10,6,'do'+':'+intToStr(length(res_data)));
        setText(frA,17,h-2,4,9,'alt+');
        setChXY(frA, 15 , h-2, color, ord('C'));
        setChXY(frA, 21 , h-2, 0, ord('1'));
        setChXY(frA, 22 , h-2, 1, ord('2'));
        setChXY(frA, 23 , h-2, 2, ord('3'));
        setChXY(frA, 24 , h-2, 3, ord('4'));
        setChXY(frA, 25 , h-2, 4, ord('5'));
        setChXY(frA, 26 , h-2, 5, ord('6'));
        setChXY(frA, 27 , h-2, 6, ord('7'));
        setChXY(frA, 28 , h-2, 7, ord('8'));
        setChXY(frA, 30 , h-2, 10, ord('-'));
        setChXY(frA, 21 , h-1, 8, ord('1'));
        setChXY(frA, 22 , h-1, 9, ord('2'));
        setChXY(frA, 23 , h-1, 10, ord('3'));
        setChXY(frA, 24 , h-1, 11, ord('4'));
        setChXY(frA, 25 , h-1, 12, ord('5'));
        setChXY(frA, 26 , h-1, 13, ord('6'));
        setChXY(frA, 27 , h-1, 14, ord('7'));
        setChXY(frA, 28 , h-1, 15, ord('8'));
        setChXY(frA, 30 , h-1, 10, ord('='));
        screen_update(frA, frB);
        delay(20);
      until reload or exit_editor;
    end;
  end;
  clrscr;
until not reload;
for n := 0 to length(res_data)-1 do log_msg('res_data',intToStr(res_data[n]));
for n := 0 to length(res_dataOffset)-1 do log_msg('res_dataOffset',intToStr(res_dataOffset[n]));
end;
