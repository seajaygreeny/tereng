procedure ascii_art_file_new(path : ansistring);
var
  data : daint;
begin
  setlength(data, 9);
  data[0] := 0;
  data[1] := 0;
  data[2] := 0;
  data[3] := 0;
  data[4] := 0;
  data[5] := 0;
  data[6] := 0;
  data[7] := 1;
  data[8] := ord('+');
  array_save(path, data);
end;
