
procedure text_art_import (
  path : ansistring;
  var data : daint;
  var dataOffset : dalw;
  var id_association : dastr;
  association : string
);
var
  f : text;
  i, k, ds, ofs : longword;
  x, y, s, id : word;
  ch : char;
begin
  ofs := length (data);
  k := ofs;
  id := length (dataOffset);
  setlength(dataOffset, id + 1);
  dataOffset[id] := ofs;
  log_msg('bugs2', 'ofs'+inttostr(ofs));
  log_msg('bugs2', 'id'+inttostr(id));
  assign (f, path);
  reset (f);
  setlength (data, ofs + 6);
  data[k] := id;
  data[k+2] := 0;
  data[k+3] := 0;
  data[k+4] := 0;
  data[k+5] := 0;
  k := k + 6;
  ds := 4;
  y := 0;
  x := 0;
  repeat
    read(f,ch);
    case ch of
      #10 : begin
        y := y + 1;
        x := 0;
      end;
      else begin
        x := x + 1;
        if (ch in [#33..#126]) or (ch in [#128..#255])
          then begin
            setlength (data, k + 5);
            ds := ds + 5;
            data[k] := 1;
            data[k+1] := x;
            data[k+2] := y;
            data[k+3] := 7;
            data[k+4] := ord(ch);
            k := k + 5;
          end;
      end;
    end;
  until (EOF(F));
  data[ofs + 1] := ds;
end;
{begin
  id := length (id_association);
  ofs := length (data);
  assign (f, path);
  reset (f);
  setlength (data, ofs + 6);
  k := ofs;
  data[k] := id;
  data[k+2] := 0;
  data[k+3] := 0;
  data[k+4] := 0;
  data[k+5] := 0;
  k := k + 6;
  y := 0;
  fs := 4;
  str := '';
  writeln('1');
  readln(f, str);
  repeat
    writeln('2');
    i := 1;
    x := 0;
    repeat
      writeln('3');
      setlength (data, k + 5);
      writeln('5');
      writeln('7 :"',str,'"');
      writeln('6',length(str),' ',i);
      writeln(str[i]);
      ch := str[i];
      writeln(ch);
      writeln('8');
      s := ord(ch);
      fs := fs + 1;
      writeln('4');
      data[k] := 1;
      data[k+1] := x;
      data[k+2] := y;
      data[k+3] := 7;
      data[k+4] := s;
      k := k + 5;
      x := x + 1;
      i := i + 1;
    until (i = length(str));
    y := y + 1;
    readln(f, str);
  until EOF(F);
  data[ofs+1] := fs;
end;
}
