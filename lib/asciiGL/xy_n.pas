procedure NtXY(var x, y : integer; n, w, h : integer);
begin
  if (n < w*h) and (n >= 0)
  then begin
    x := n mod (w - 0);
    y := n div (w - 0);
  end
  else begin
    x := 0;
    y := 0;
  end;
end;

function XYtN(x, y, w, h : integer) : integer;
begin
  if (h > y) and (w > x) and (0 <= y) and (0 <= x)
  then XYtN := w * y + x;
  //else XYtN := 0;
end;