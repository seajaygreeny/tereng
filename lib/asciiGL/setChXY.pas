procedure setChXY(var frame : array of integer; x, y, color, ch : integer);
var i : integer;
begin
  if (x >= 0) and (x < frame[2]) and (y >= 0) and (y < frame[3])
  then begin
    i := XYtN(x,y,frame[2],frame[3]);
    case frame[0] of
    0: begin
         frame[i+frame[1]] := ch;
       end;
    1: begin
         frame[i*2+frame[1]] := ch;
         frame[i*2+frame[1]+1] := color;
        // gotoxy(20,i);
        // write('\',i);
       end;
     end;
  end;
end;