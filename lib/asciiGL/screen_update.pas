// Ahhh !!! I spent 7 hours before the code to find here the absence of "var" before declaring arrays A and B. >_(\
procedure screen_update(var frameA, frameB : array of integer);  
var
  i : integer;
begin
  if (length(frameA) = length(frameB)) and (frameA [0] = frameB [0]) and (frameA[1] = frameB[1])
  then begin
    case frameA[0] of
    0: begin
         {for i:= 4 to length(frameA) do begin
          if frameA[i] = frameB[i]
          then frameA[i] := -1
          else begin
            frameB[i] := frameA[i];
            printChN(i,frameA[i*2],frameA[i*2+1], frameA[2], frameA[3]);
          end;
         end;}
    end;
    1: begin
         for i:= 0 to frameA[2]*frameA[3]-1 do begin
           if frameA[i*2+frameA[1]] = frameB[i*2+frameA[1]]
             then if frameA[i*2+frameA[1]+1] = frameB[i*2+frameA[1]+1]
               then begin
                 frameA[i*2+frameA[1]] := 0;
                 frameA[i*2+frameA[1]+1] := 0;
               end
               else begin
                 frameB[i*2+frameA[1]+1] := frameA[i*2+frameA[1]+1];
                 printChN(i, frameA[i*2+frameA[1]], frameA[i*2+frameA[1]+1], frameA[2], frameA[3]); 
                 frameA[i*2+frameA[1]] := 0;
                 frameA[i*2+frameA[1]+1] := 0;
               end
             else begin
               frameB[i*2+frameA[1]] := frameA[i*2+frameA[1]];
               frameB[i*2+frameA[1]+1] := frameA[i*2+frameA[1]+1];
               printChN(i, frameA[i*2+frameA[1]], frameA[i*2+frameA[1]+1], frameA[2], frameA[3]);
               frameA[i*2+frameA[1]] := 0;
               frameA[i*2+frameA[1]+1] := 0;
             end;
           end;
       end;
     end;
  end
  else begin
    textbackground(0);
    textcolor(7);
    gotoxy(2,2);
    write('asciiGL_screen_update: frameA != frameB');
  end;
  textcolor(7);
end;
