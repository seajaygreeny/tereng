procedure initFrame(var frame : daint; format: byte; w, h : word);
var 
  n : longint;
begin
  case format of
  0 : begin 
        setlength(frame, (w*h)+4);
      end;
  1 : begin 
        setlength(frame, (w*h*2)+4);
      end;
  end;
  for n:= 4 to length(frame)-1 do frame [n] := 0;
  frame[0] := format;
  frame[1] := 4;
  frame[2] := w;
  frame[3] := h;
end;
