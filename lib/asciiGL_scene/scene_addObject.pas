procedure scene_addObject (var scene : daint; id_associations : dastr; association : string; x, y, z, d, m : integer);
var
  i, id : word;
begin
  id := get_idAssociation(id_associations, association);
  i := length(scene);
  setlength(scene, i + 6);
  scene[i] := id;
  scene[i+1] := x;
  scene[i+2] := y;
  scene[i+3] := z;
  scene[i+4] := d;
  scene[i+5] := m;
end;
