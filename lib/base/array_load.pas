procedure array_load (
	path : ansistring;
	var arr : daint
);
var
  f : file of integer;
  i : longword;
begin
  assign(f, path);
  reset(f);
  i := 0;
  setlength(arr, filesize(f));
  repeat
    read(f, arr[i]);
    i := i + 1;
  until i >= length(arr);
  close(f);
end;

procedure load_array (
	path : ansistring;
	var arr : dastr
);
var
  f : file of string;
  i : longword;
begin
  assign(f, path);
  reset(f);
  i := 0;
  setlength(arr, filesize(f));
  repeat
    read(f, arr[i]);
    i := i + 1;
  until i >= length(arr);
  close(f);
end;
