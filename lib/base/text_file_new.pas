procedure text_file_new (path : ansistring);
var
  f : text;
begin
  assign(f, path);
  rewrite(f);
  close(f);
end;
