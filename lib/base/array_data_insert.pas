procedure array_data_insert (
  var data: daint;
  insert_data: daint;
  insert_point: longword
);
var
  length_ins, length_dat, n : longword;
begin
  length_ins := length(insert_data);
  length_dat := length(data) + length_ins;
  setlength(data, length_dat);
  for n := length_dat-1 downto insert_point do begin
    data[n] := data[n-length_ins];
  end;
  for n:= 0 to length_ins-1 do begin
    data[n+insert_point] := insert_data[n];
  end;
end;
