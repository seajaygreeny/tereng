uses crt;
var 
  frameA, frameB, scene, 
  objects_models_data,
  objects_models_dataOffset : array of integer;
  objects_models_idAssociation : array of string;
  n : integer;
type
  daint = array of integer;
  dastr = array of string;
  {$I lib/asciiGL/asciiGL.inc}
  {$I lib/asciiGL_scene/asciiGL_scene.inc}
begin
  ///  init
  initFrame(frameA, 1, 80, 24);
  initFrame(frameB, 1, 80, 24);
  setlength(scene,0);
  setlength(objects_models_data,0); 
  setlength(objects_models_idAssociation,0);
  setlength(objects_models_dataOffset,0);
  ///  init_end
  dataLoad (objects_models_data, objects_models_dataOffset,  objects_models_idAssociation, 'test_data/test_2.dat', 'mod');
  dataLoad (objects_models_data, objects_models_dataOffset, objects_models_idAssociation, 'test_data/test_2.dat', 'mod2');
  for n := 0 to length(objects_models_data)-1 do begin
    writeln(n, '- ', objects_models_data[n]);
  end;
  for n := 0 to length(objects_models_dataOffset)-1 do begin
    writeln(n, '- ', objects_models_dataOffset[n]);
  end;
  for n := 0 to length(objects_models_idAssociation)-1 do begin
    writeln(n, '- ', objects_models_idAssociation[n]);
  end;
  scene_addObject (scene, objects_models_idAssociation, 'mod2', 0,0,0,0,0);
  for n := 0 to length(scene)-1 do begin
    writeln(n, '- ', scene[n]);
  end;
  model_rend( frameA, objects_models_data, objects_models_dataOffset, 0, 10, 7, 0, 0);
  scene_addObject (scene, objects_models_idAssociation, 'mod', 0,0,0,0,0);
  scene_addObject (scene, objects_models_idAssociation, 'mod', 4,0,0,0,0);
  scene_addObject (scene, objects_models_idAssociation, 'mod', 5,0,1,0,0);
  scene_addObject (scene, objects_models_idAssociation, 'mod', 7,0,2,0,0);
  scene_addObject (scene, objects_models_idAssociation, 'mod', 6,0,1,0,0);
  scene_rend (frameA , scene, objects_models_data, objects_models_dataOffset, 0, 0);
  // rendScene (frameA, scene);
  screen_update(frameA, frameB);
  readkey;
end.
